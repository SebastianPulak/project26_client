
package milionerzy;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class NewJFrame extends javax.swing.JFrame {

    private static String correctAnswerNumber;

    public NewJFrame() throws IOException {
        initComponents();

        jNewgameButton.setVisible(true);
        jExitButton1.setVisible(true);
        jAnswer1Button.setVisible(false);
        jAnswer2Button.setVisible(false);
        jAnswer3Button.setVisible(false);
        jAnswer4Button.setVisible(false);
        jCashoutButton.setVisible(false);
        jHelp1Button.setVisible(false);
        jHelp2Button.setVisible(false);
        jHelp3Button.setVisible(false);
        jMainLabel.setVisible(false);
        jHelpLabel.setVisible(false);
        jPrize5Label.setVisible(false);
        jPrize1Label.setVisible(false);
        jPrize2Label.setVisible(false);
        jPrize2Label.setVisible(false);
        jPrize3Label.setVisible(false);
        jPrize4Label.setVisible(false);
        jPrize11Label.setVisible(false);
        jPrize6Label.setVisible(false);
        jPrize7Label.setVisible(false);
        jPrize8Label.setVisible(false);
        jPrize9Label.setVisible(false);
        jPrize10Label.setVisible(false);
        jPrize12Label.setVisible(false);

        //przypisuje kolejne PrizeLabele do kolejnych elementów tablicy, co ułatwi mi podświetlanie kwoty o która aktualnie gramy
        prizeLabelTab[0] = jPrize1Label;
        prizeLabelTab[1] = jPrize2Label;
        prizeLabelTab[2] = jPrize3Label;
        prizeLabelTab[3] = jPrize4Label;
        prizeLabelTab[4] = jPrize5Label;
        prizeLabelTab[5] = jPrize6Label;
        prizeLabelTab[6] = jPrize7Label;
        prizeLabelTab[7] = jPrize8Label;
        prizeLabelTab[8] = jPrize9Label;
        prizeLabelTab[9] = jPrize10Label;
        prizeLabelTab[10] = jPrize11Label;
        prizeLabelTab[11] = jPrize12Label;

        this.getContentPane().setBackground(Color.black);

    }
        
    
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jNewgameButton = new javax.swing.JButton();
        jCashoutButton = new javax.swing.JButton();
        jHelp3Button = new javax.swing.JButton();
        jHelp2Button = new javax.swing.JButton();
        jHelp1Button = new javax.swing.JButton();
        jAnswer2Button = new javax.swing.JButton();
        jAnswer4Button = new javax.swing.JButton();
        jAnswer1Button = new javax.swing.JButton();
        jAnswer3Button = new javax.swing.JButton();
        jPrize8Label = new javax.swing.JLabel();
        jPrize4Label = new javax.swing.JLabel();
        jPrize7Label = new javax.swing.JLabel();
        jPrize3Label = new javax.swing.JLabel();
        jPrize9Label = new javax.swing.JLabel();
        jPrize5Label = new javax.swing.JLabel();
        jPrize10Label = new javax.swing.JLabel();
        jPrize12Label = new javax.swing.JLabel();
        jPrize1Label = new javax.swing.JLabel();
        jPrize11Label = new javax.swing.JLabel();
        jPrize6Label = new javax.swing.JLabel();
        jPrize2Label = new javax.swing.JLabel();
        jExitButton1 = new javax.swing.JButton();
        jMainLabel = new javax.swing.JLabel();
        jHelpLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Milionerzy");
        setBackground(new java.awt.Color(0, 204, 0));
        setPreferredSize(new java.awt.Dimension(1366, 768));

        jNewgameButton.setBackground(new java.awt.Color(0, 0, 0));
        jNewgameButton.setForeground(new java.awt.Color(255, 255, 255));
        jNewgameButton.setText("Nowa Gra");
        jNewgameButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jNewgameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jNewgameButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        jCashoutButton.setBackground(new java.awt.Color(0, 0, 0));
        jCashoutButton.setForeground(new java.awt.Color(255, 255, 255));
        jCashoutButton.setText("Rezygnuje");
        jCashoutButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jCashoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCashoutButtonActionPerformed(evt);
            }
        });

        jHelp3Button.setBackground(new java.awt.Color(0, 0, 0));
        jHelp3Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jHelp3Button.setForeground(new java.awt.Color(255, 255, 255));
        jHelp3Button.setText("Telefon");
        jHelp3Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jHelp3Button.setPreferredSize(new java.awt.Dimension(79, 23));
        jHelp3Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jHelp3ButtonActionPerformed(evt);
            }
        });

        jHelp2Button.setBackground(new java.awt.Color(0, 0, 0));
        jHelp2Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jHelp2Button.setForeground(new java.awt.Color(255, 255, 255));
        jHelp2Button.setText("Publiczność");
        jHelp2Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jHelp2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jHelp2ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        jHelp1Button.setBackground(new java.awt.Color(0, 0, 0));
        jHelp1Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jHelp1Button.setForeground(new java.awt.Color(255, 255, 255));
        jHelp1Button.setText("50/50");
        jHelp1Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jHelp1Button.setPreferredSize(new java.awt.Dimension(79, 23));
        jHelp1Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jHelp1ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        jAnswer2Button.setBackground(new java.awt.Color(0, 0, 0));
        jAnswer2Button.setForeground(new java.awt.Color(255, 255, 255));
        jAnswer2Button.setText("B.");
        jAnswer2Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jAnswer2Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 102), 2));
        jAnswer2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jAnswer2ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        jAnswer4Button.setBackground(new java.awt.Color(0, 0, 0));
        jAnswer4Button.setForeground(new java.awt.Color(255, 255, 255));
        jAnswer4Button.setText("D.");
        jAnswer4Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jAnswer4Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 102), 2));
        jAnswer4Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jAnswer4ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        jAnswer1Button.setBackground(new java.awt.Color(0, 0, 0));
        jAnswer1Button.setForeground(new java.awt.Color(255, 255, 255));
        jAnswer1Button.setText("A.");
        jAnswer1Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jAnswer1Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 102), 2));
        jAnswer1Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jAnswer1ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        jAnswer3Button.setBackground(new java.awt.Color(0, 0, 0));
        jAnswer3Button.setForeground(new java.awt.Color(255, 255, 255));
        jAnswer3Button.setText("C.");
        jAnswer3Button.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 14)); // NOI18N
        jAnswer3Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 102), 2));
        jAnswer3Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    jAnswer3ButtonActionPerformed(evt);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        jPrize8Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize8Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize8Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize8Label.setText("     75 000");
        jPrize8Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize4Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize4Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize4Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize4Label.setText("        5000");
        jPrize4Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize7Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize7Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize7Label.setForeground(new java.awt.Color(255, 255, 255));
        jPrize7Label.setText("     40 000");
        jPrize7Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize3Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize3Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize3Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize3Label.setText("        2000");
        jPrize3Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize9Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize9Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize9Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize9Label.setText("   125 000");
        jPrize9Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize5Label.setBackground(new java.awt.Color(0, 0, 0));
        jPrize5Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize5Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize5Label.setText("     10 000");
        jPrize5Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize10Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize10Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize10Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize10Label.setText("   250 000");
        jPrize10Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize12Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize12Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize12Label.setForeground(new java.awt.Color(255, 255, 255));
        jPrize12Label.setText("1 000 000");
        jPrize12Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize1Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize1Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize1Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize1Label.setText("          500");
        jPrize1Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize11Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize11Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize11Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize11Label.setText("   500 000");
        jPrize11Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize6Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize6Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize6Label.setForeground(new java.awt.Color(153, 153, 0));
        jPrize6Label.setText("     20 000");
        jPrize6Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jPrize2Label.setBackground(new java.awt.Color(0, 153, 255));
        jPrize2Label.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jPrize2Label.setForeground(new java.awt.Color(255, 255, 255));
        jPrize2Label.setText("        1000");
        jPrize2Label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        jExitButton1.setBackground(new java.awt.Color(0, 0, 0));
        jExitButton1.setForeground(new java.awt.Color(255, 255, 255));
        jExitButton1.setText("Wyjście");
        jExitButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));
        jExitButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jExitButton1ActionPerformed(evt);
            }
        });

        jMainLabel.setBackground(new java.awt.Color(0, 0, 0));
        jMainLabel.setText(".");
        jMainLabel.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 18)); // NOI18N
        jMainLabel.setForeground(Color.white);
        jMainLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));

        jHelpLabel.setBackground(new java.awt.Color(0, 0, 0));
        jHelpLabel.setText(".");
        jHelpLabel.setFont(new java.awt.Font("Yu Gothic UI Light", 1, 18)); // NOI18N
        jHelpLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 2));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jHelp3Button, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jHelp2Button, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jNewgameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jExitButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jAnswer1Button, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGap(358, 358, 358)
                                    .addComponent(jHelp1Button, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(252, 252, 252)
                                    .addComponent(jCashoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jHelpLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jMainLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jAnswer3Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(80, 80, 80)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jAnswer4Button, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                                        .addComponent(jAnswer2Button, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 229, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jPrize4Label)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPrize6Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize7Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize8Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize11Label, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPrize10Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize12Label, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPrize9Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize5Label, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPrize3Label, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPrize2Label, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addComponent(jPrize1Label, javax.swing.GroupLayout.Alignment.TRAILING)))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPrize10Label, jPrize11Label, jPrize1Label, jPrize2Label, jPrize3Label, jPrize4Label, jPrize5Label, jPrize6Label, jPrize7Label, jPrize8Label, jPrize9Label});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jExitButton1, jNewgameButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jExitButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCashoutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jNewgameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jHelp3Button, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jHelp2Button)
                        .addComponent(jHelp1Button, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jHelpLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(jMainLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jAnswer1Button, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jAnswer2Button))
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jAnswer3Button)
                            .addComponent(jAnswer4Button)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPrize12Label, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize11Label, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize10Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize9Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize8Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize7Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize6Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize5Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize4Label)
                        .addGap(18, 18, 18)
                        .addComponent(jPrize3Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize2Label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPrize1Label)))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jPrize10Label, jPrize11Label, jPrize1Label, jPrize2Label, jPrize3Label, jPrize4Label, jPrize5Label, jPrize6Label, jPrize7Label, jPrize8Label, jPrize9Label});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jCashoutButton, jExitButton1, jHelp1Button, jHelp2Button, jHelp3Button, jNewgameButton});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jAnswer1Button, jAnswer2Button, jAnswer3Button, jAnswer4Button});

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void jAnswer1ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InterruptedException {//GEN-FIRST:event_jAnswer1ButtonActionPerformed
        String thisButtonAnswerNumber = "1";
        question=GameLogic.checkAnswer(thisButtonAnswerNumber,correctAnswerNumber,question, jMainLabel,jAnswer1Button, jAnswer2Button, jAnswer3Button, jAnswer4Button, prizeLabelTab,jHelpLabel,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);
    }//GEN-LAST:event_jAnswer1ButtonActionPerformed

    private void jAnswer2ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InterruptedException {//GEN-FIRST:event_jAnswer2ButtonActionPerformed
        String thisButtonAnswerNumber = "2";
        question=GameLogic.checkAnswer(thisButtonAnswerNumber,correctAnswerNumber,question, jMainLabel,jAnswer1Button, jAnswer2Button, jAnswer3Button, jAnswer4Button, prizeLabelTab,jHelpLabel,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);

    }//GEN-LAST:event_jAnswer2ButtonActionPerformed


    private void jAnswer3ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InterruptedException {//GEN-FIRST:event_jAnswer1ButtonActionPerformed
        String thisButtonAnswerNumber = "3";
        question=GameLogic.checkAnswer(thisButtonAnswerNumber,correctAnswerNumber,question, jMainLabel,jAnswer1Button, jAnswer2Button, jAnswer3Button, jAnswer4Button, prizeLabelTab,jHelpLabel,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);

    }//GEN-LAST:event_jAnswer3ButtonActionPerformed


    private void jAnswer4ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InterruptedException {//GEN-FIRST:event_jAnswer1ButtonActionPerformed
        String thisButtonAnswerNumber = "4";
        question=GameLogic.checkAnswer(thisButtonAnswerNumber,correctAnswerNumber,question, jMainLabel,jAnswer1Button, jAnswer2Button, jAnswer3Button, jAnswer4Button, prizeLabelTab,jHelpLabel,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);

    }//GEN-LAST:event_jAnswer4ButtonActionPerformed

    private void jCashoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCashoutButtonActionPerformed
        GameLogic.cashOutButton(correctAnswerNumber,question,jHelpLabel,prizeLabelTab,jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);
    }//GEN-LAST:event_jCashoutButtonActionPerformed

    private void jNewgameButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException {//GEN-FIRST:event_jNewgameButtonActionPerformed

        if (newGameClickCounter != 0){
            question.setCounter(0);
        }

        newGameClickCounter++;

        jAnswer1Button.setVisible(true);
        jAnswer2Button.setVisible(true);
        jAnswer3Button.setVisible(true);
        jAnswer4Button.setVisible(true);
        jCashoutButton.setVisible(true);
        jHelp1Button.setVisible(true);
        jHelp2Button.setVisible(true);
        jHelp3Button.setVisible(true);
        jMainLabel.setVisible(true);
        jHelpLabel.setVisible(true);

        jAnswer1Button.setForeground(Color.white);
        jAnswer2Button.setForeground(Color.white);
        jAnswer3Button.setForeground(Color.white);
        jAnswer4Button.setForeground(Color.white);

        jAnswer1Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));
        jAnswer2Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));
        jAnswer3Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));
        jAnswer4Button.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));

        GameLogic.buttonsAvaliability(true,jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button,jCashoutButton,jHelp1Button,jHelp2Button,jHelp3Button);

        for (int i=0; i < 12; i++){
            prizeLabelTab[i].setVisible(true);
            prizeLabelTab[i].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 51), 2));
        }

        question=GameLogic.newQuestion(jMainLabel,jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button, prizeLabelTab, jHelpLabel);
        correctAnswerNumber = question.getCorrect_Answer();
        jHelpLabel.setForeground(Color.green);
        jHelpLabel.setText("Przed Tobą pytanie za 500 złotych.");

    }//GEN-LAST:event_jNewgameButtonActionPerformed

    private void jHelp1ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException {//GEN-FIRST:event_jHelp1ButtonActionPerformed
        GameLogic.HelpButton1(question, jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button,jHelp1Button);
    }//GEN-LAST:event_jHelp1ButtonActionPerformed

    private void jHelp2ButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException {//GEN-FIRST:event_jHelp1ButtonActionPerformed
        GameLogic.HelpButton2(question, jHelpLabel, jHelp2Button,jHelp1Button,jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button);
    }//GEN-LAST:event_jHelp1ButtonActionPerformed

    private void jHelp3ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jHelp3ButtonActionPerformed
        GameLogic.HelpButton3(question, jHelpLabel, jHelp3Button,jHelp1Button,jAnswer1Button,jAnswer2Button,jAnswer3Button,jAnswer4Button);
    }//GEN-LAST:event_jHelp3ButtonActionPerformed

    private void jExitButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jExitButton1ActionPerformed
    System.exit(0);
    }//GEN-LAST:event_jExitButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new NewJFrame().setVisible(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jAnswer1Button;
    private javax.swing.JButton jAnswer2Button;
    private javax.swing.JButton jAnswer3Button;
    private javax.swing.JButton jAnswer4Button;
    private javax.swing.JButton jCashoutButton;
    private javax.swing.JButton jExitButton1;
    private javax.swing.JLabel jPrize1Label;
    private javax.swing.JLabel jPrize2Label;
    private javax.swing.JLabel jPrize3Label;
    private javax.swing.JLabel jPrize4Label;
    private javax.swing.JLabel jPrize5Label;
    private javax.swing.JLabel jPrize6Label;
    private javax.swing.JLabel jPrize7Label;
    private javax.swing.JLabel jPrize8Label;
    private javax.swing.JLabel jPrize9Label;
    private javax.swing.JLabel jPrize10Label;
    private javax.swing.JLabel jPrize11Label;
    private javax.swing.JLabel jPrize12Label;
    private javax.swing.JButton jHelp1Button;
    private javax.swing.JButton jHelp2Button;
    private javax.swing.JButton jHelp3Button;
    private javax.swing.JLabel jHelpLabel;
    private javax.swing.JLabel jMainLabel;
    private javax.swing.JButton jNewgameButton;
    // End of variables declaration//GEN-END:variables


    Question question;

    int newGameClickCounter = 0;

    JLabel[] prizeLabelTab=new JLabel[12];

    public static void setCorrectAnswerNumber(String a){
        correctAnswerNumber=a;
    }
}
